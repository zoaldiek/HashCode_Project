package ihm;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PanelFond extends JPanel {
	
    private static final long serialVersionUID = 6594448854168315322L;
    private BufferedImage image;
    
    public PanelFond(String nomImage, Color couleur){
        changeImage(nomImage);
        setBackground(couleur);
    }

    @Override
    public void paintComponent(Graphics g){
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());
        g.drawImage(image, (getWidth()-image.getWidth())/2, 20, null);
        g.setColor(getForeground());
        g.setFont(new Font("Calibri", Font.BOLD, 30));
    }
    
    public void changeImage(String nomImage){
    	try {
            image = ImageIO.read(new File(nomImage));
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }
   
}
