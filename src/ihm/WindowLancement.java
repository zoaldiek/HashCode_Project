package ihm;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

public class WindowLancement extends JWindow implements Runnable{

	private static final long serialVersionUID = -784661957617542324L;

	private JLabel liste;
	private JProgressBar progressBar;
	private PanelFond panelCentre;
	
	public WindowLancement(String nomImage, Color couleur){
		setLayout(new BorderLayout());
		
		panelCentre = new PanelFond(nomImage, couleur);
		add(panelCentre);
		JPanel panelSud = new JPanel(new BorderLayout());
		panelSud.setOpaque(false);
		
		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setBackground(couleur);
		progressBar.setForeground(Color.LIGHT_GRAY);
		
		liste = new JLabel("");
		liste.setForeground(Color.BLACK);
		liste.setBackground(couleur);
		liste.setOpaque(true);
		panelSud.add(progressBar, BorderLayout.SOUTH);
		panelSud.add(liste, BorderLayout.CENTER);
		add(panelSud, BorderLayout.SOUTH);
		setSize(687, 250);
		setLocationRelativeTo(null);
	}

	public void avancer(int avancement, String etape){
		setInfo(etape);
		setAvancement(avancement);
		repaint();
	}

	public void setInfo(String info){
		liste.setText("  "+info);
	}

	public void setAvancement(int value){
		progressBar.setValue(value);
	}

	public void changeImage(String nomImage) {
		panelCentre.changeImage(nomImage);
	}

	@Override
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		repaint();
		panelCentre.repaint();
	}

}
