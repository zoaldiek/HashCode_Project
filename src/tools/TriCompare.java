package tools;

import java.util.Comparator;

import principale.Server;

public class  TriCompare implements Comparator<Server>{

	@Override
	public int compare(Server arg0, Server arg1) {
		return new Integer(arg0.getCapacite()).compareTo(arg1.getCapacite());
	}

}
