package tools;

import java.util.Comparator;

import principale.Server;

public class TriServeur implements Comparator<Server> {

	@Override
	public int compare(Server o1, Server o2) {		
		return new Integer(o1.getNbEmpacement()).compareTo(o2.getNbEmpacement());
	}

}
