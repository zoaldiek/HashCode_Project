package tools;

import java.util.Comparator;

import principale.Server;

public class TriNbEmplacement implements Comparator<Server>{

	@Override
	public int compare(Server arg0, Server arg1) {
		return new Integer(arg0.getNbEmpacement()).compareTo(arg1.getNbEmpacement());
	}

}