package tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import algorithmes.Glouton;

import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import principale.DataStruct;
import principale.Server;
import principale.Slot;

public class AlgoGenetique {
	//taille = nombre de Data par popupalations
	private int TAILLE = 50;
	//Nombre de tour
	private int NBTOUR = 5000;
	//Nombre de populations par génération
	private int NBGENERATIONS = 20;
	//Probabilité de mutation
	private double PROBAMUTATION = 0.1;
	//Nombre de fois qu'une stagnation est autorisé pour une même génération
	private int CONVERGENCEGENERATIONS = 30;

	private HashMap<Integer,DataStruct> populations;

	private ArrayList<Integer> scorePopulations;
	private DataStruct nonInitialisedData;

	private ArrayList<DataStruct> listrand = new ArrayList<>();


	public AlgoGenetique(DataStruct data)
	{
		nonInitialisedData = data;
		populations = new HashMap<>();

		//on fait de la génération aléatoire de population en partant de la data originale
		for(int i=0;i < 100;i++)
		{
			DataStruct ndata = this.RandomiseAffect(nonInitialisedData);

			listrand.add(ndata);
		}
		Collections.shuffle(listrand);
		for(int i = 0;i < TAILLE - TAILLE/5 ;i++)
		{
			DataStruct ndata = listrand.get(i);
			populations.put(ndata.score(), ndata);
		}
		DataStruct newdata = Glouton.Perso(data);
		System.out.println(newdata.score());
		for(int i = 0;i<TAILLE/5;i++)
		{
			DataStruct ndata;
			try {
				ndata = (DataStruct) newdata.clone();

				populations.put(ndata.score(), ndata);
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void Show(HashMap<Integer,DataStruct> pops)
	{

		SortedSet<Integer> keys = new TreeSet  <Integer>(pops.keySet());
		for(Integer k : keys)
		{
			System.out.println(k);
		}
		System.out.println("ce que j'en pense "+keys.last());

	}

	@SuppressWarnings("unchecked")
	public DataStruct run() throws CloneNotSupportedException
	{
		int i = 0;
		int nbStagnation = 0;
		ArrayList<HashMap<Integer,DataStruct>> generations = new ArrayList<>();
		generations.add(populations);
		while(i<NBTOUR && nbStagnation < CONVERGENCEGENERATIONS )
		{
			//on verifie si sur 10 génération on a eu une évolution
			if(!generations.isEmpty() && generations.size() > NBGENERATIONS)
			{
				SortedSet<Integer> keys = new TreeSet  <Integer>(generations.get(0).keySet());
				int max = keys.last();
				int indiceGenerations = 0;
				for(int j =1;j<generations.size();j++)
				{
					for(Integer score : generations.get(j).keySet())
					{
						if(max < score)
						{
							max = score;
							indiceGenerations = j;
						}
					}

				}
				//on regarde si on a eu une population meilleure parmis toutes les générations
				//si oui cette meilleure populations devient la première génération
				//sinon on repart de la population initiale
				if(max > keys.last())
				{
					System.out.println("meilleure population trouvée score : "+max);
					HashMap<Integer,DataStruct> tmpPops = generations.get(indiceGenerations);
					generations.clear();
					generations.add(tmpPops);
					nbStagnation = 0;
				}else{
					System.out.println("stagnation on retourne à l'ancienne meilleure génération score : "+max);
					populations = generations.get(0);
					generations.clear();
					generations.add(populations);
					nbStagnation++;
				}
			}

			//on trie la populations actuelle 
			SortedSet<Integer> keys = new TreeSet  <Integer>(populations.keySet());
			Iterator<Integer> itr = ((TreeSet<Integer>) keys).descendingIterator();
			//			System.out.println(keys.last());
			//on met en place de nouvelles populations
			HashMap<Integer,DataStruct> newPop = new HashMap<>();

			/*
			 * On recupere les 40 meilleurs populations
			 * 		CROISEMENTS				
			 * pour les 10 premieres populations , on selection aléatoirement un numéro de serveur 
			 * on regarde où se situe ce serveur sur les autres serveurs et on l'affecte au meme emplacement
			 * si l'emplacement n'est pas libre on enleve les serveurs present des serveurs affectés
			 * 		Mutations
			 *	pour les 30 autres serveurs , on effectue une action de mutation
			 * 3 types de mutations : enlever un serveur , chanfer l'affectation d'un pool, placé un serveur sur un slot disponible
			 */


			ArrayList<DataStruct> topTen = new ArrayList<>();
			ArrayList<DataStruct> topUnderTen = new ArrayList<>();
			Random rand = new Random();
			int j = 0;
			while(itr.hasNext() && j<40 )
			{
				Integer key = itr.next();
				DataStruct tmpData = populations.get(key);
				if(j<10)
				{
					topTen.add(tmpData);
				}else{
					topUnderTen.add(tmpData);
				}
				j++;
			}

			//on choisi une population aléatoirement dans le top 10
			for(j = 0;j<topTen.size();j++)
			{

				int indiceServer = rand.nextInt(topTen.get(j).getListServer().size());

				DataStruct dataThisTopTen = (DataStruct) topTen.get(j).clone();
				Server serverThisTopTen = dataThisTopTen.getListServer().get(indiceServer);


				// on choisi un des autres topten au hasard
				ArrayList<DataStruct> ldata = (ArrayList<DataStruct>) topTen.clone();
				ldata.remove(dataThisTopTen);
				int indiceOtherTopTen = rand.nextInt(ldata.size()-1);

				DataStruct dataOtherTopTen = ldata.get(indiceOtherTopTen);
				Server serverOtherTopTen = this.RecupServeurById(dataOtherTopTen, serverThisTopTen.getId());
				//on cherche le serveur a la meme ID


				// et on cherche à placer le serveur present dans ThisTopTen à la meme place que dans OtherTopTen

				if(serverOtherTopTen.getRangee() == -1)
				{
					//si le server n'est pas affacté on l'enleve et on l'enleve de sa pool et on rend son slot disponible
					//on verifie si il n'est pas déjà non affecté a un slot dans dataThisTopTen
					if(serverThisTopTen.getRangee() != -1)
					{
						this.popServer(dataThisTopTen, serverThisTopTen);
					}

				}else{
					//le serveur doit etre placé sur le meme emplacement que celui de OtherTopTen
					//on regarde si l'emplacement est disponible , si non on enleve les serveurs déja présent
					for(int k = 0;k<serverThisTopTen.getNbEmpacement()-1;k++)
					{
						if(!dataThisTopTen.getListFreeSlots().get(serverOtherTopTen.getRangee()).get(serverOtherTopTen.getSlot()+k))
						{
							//le slot n'est pas dispo
							//le serveur dans ce slot doit donc devenir non affecté
							ArrayList<Integer> listServPossibleACeSlot = dataThisTopTen.getListServerParSlots().get(serverOtherTopTen.getRangee()).get(serverOtherTopTen.getSlot()+k);
							//on cherche parmis les serveur possibles si ceux ci sont affecté a ce slot
							for(Integer id : listServPossibleACeSlot)
							{
								Server serv = this.RecupServeurById(dataThisTopTen, id);
								//si un des serveurs possible est affecté alors on le Pop des serveurs affecté
								if(serv.getRangee() == serverOtherTopTen.getRangee() && serv.getSlot() == serverOtherTopTen.getSlot()+k )
								{
									this.popServer(dataThisTopTen, serv);
								}
							}

						}
					}
					//une fois la place libérée on l'affecte au meme emplacement en gardant la meme afectation de pool
					serverThisTopTen.setRangee(serverOtherTopTen.getRangee());
					serverThisTopTen.setSlot(serverOtherTopTen.getSlot());
					serverThisTopTen.setPool(serverOtherTopTen.getPool());
				}
				//on ajoute cette data a la nouvelle population
				topTen.set(j, dataThisTopTen);
				newPop.put(dataThisTopTen.score(), dataThisTopTen);
			}


			//on tente de faire muter les autres populations de 11 à 40
			// avec un pourcentage faible

			for(j=0;j<topUnderTen.size();j++)
			{
				DataStruct newData = (DataStruct) topUnderTen.get(j).clone();
				double proba = rand.nextDouble();
				if(proba < PROBAMUTATION)
				{
					int typemuta = rand.nextInt(2);
					switch (typemuta) {
					//on enleve un élément
					case 0:

						ArrayList<Integer> listServAffect = new ArrayList<>();

						for(int serv = 0; serv<newData.getListServer().size(); serv++){
							if(newData.getListServer().get(serv).getRangee() != -1){
								listServAffect.add(newData.getListServer().get(serv).getId());
							}
						}

						int servSupp = listServAffect.get(rand.nextInt(listServAffect.size()));

						this.popServer(newData, this.RecupServeurById(newData, servSupp));

						break;

						//on change l'affectation
					case 1:

						Collections.shuffle(newData.getListServer());
						for(Server s : newData.getListServer())
						{
							if(s.getRangee() != -1)
							{
								s.setPool(rand.nextInt(newData.getNbPool()));
								break;
							}
						}
						break;

						//sinon on place un serveur encore non affecté
					case 2:

						boolean emplacementLibre = false;

						int servAjout = 0;

						listServAffect = new ArrayList<>();
						ArrayList<Slot> slotsPossible = new ArrayList<>();
						ArrayList<Slot> slotsDipo = new ArrayList<>();

						emplacementLibre = false;

						servAjout = 0;

						ArrayList<Integer> listServNonAffect = new ArrayList<>();
						slotsPossible = new ArrayList<>();
						slotsDipo = new ArrayList<>();

						for(int serv = 0; serv<newData.getListServer().size(); serv++){
							if(newData.getListServer().get(serv).getRangee() == -1){
								listServNonAffect.add(newData.getListServer().get(serv).getId());
							}
						}

						while(slotsDipo.isEmpty() && !listServNonAffect.isEmpty()){
							int randomIndex = rand.nextInt(listServNonAffect.size());
							servAjout = listServNonAffect.get(randomIndex);

							slotsPossible = newData.getListServer().get(servAjout).getListSlotsPossible();
							for(int s = 0; s<slotsPossible.size()-1; s++){
								emplacementLibre = true;
								for(int k = 0; i<newData.getListServer().get(servAjout).getNbEmpacement() && slotsPossible.get(s).getSlot()+k < newData.getSlots(); k++){
									//	System.out.println(slotsPossible.get(s).getRangee()+"  "+slotsPossible.get(s).getSlot()+k+" slots max : "+newData.getSlots());
									if(!newData.getListFreeSlots().get(slotsPossible.get(s).getRangee()).get(slotsPossible.get(s).getSlot()+k)){
										emplacementLibre = false;
									}
								}
								if(emplacementLibre){
									slotsDipo.add(slotsPossible.get(s));
								}
							}
							if(slotsDipo.isEmpty()){
								listServNonAffect.remove(randomIndex);
							}
						}

						if(!slotsDipo.isEmpty()){
							Slot slotSelect = slotsDipo.get(rand.nextInt(slotsDipo.size()));
							for(int s = 0; s<newData.getListServer().get(servAjout).getNbEmpacement(); s++){
								newData.getListFreeSlots().get(slotSelect.getRangee()).set((slotSelect.getSlot()+s), false);
							}
							newData.getListServer().get(servAjout).setPool(rand.nextInt(newData.getNbPool()));
							newData.getListServer().get(servAjout).setRangee(slotSelect.getRangee());
							newData.getListServer().get(servAjout).setSlot(slotSelect.getSlot());
						}

						break;
					default:
						break;
					}
				}
			}

			Collections.shuffle(listrand);
			for(j = 0;j < 10 ;j++)
			{
				DataStruct ndata = listrand.get(j);
				newPop.put(ndata.score(), ndata);
			}





			//on rajoute les nouvelles Pops dans les générations
			populations = newPop;
			generations.add(newPop);
			i++;

			keys = new TreeSet  <Integer>(populations.keySet());
			System.out.println("Meilleur score de toutes les populations à l'itération  : "+(i-1)+" score : "+populations.get(keys.last()).score());
		}
		int indiceGenerations = 0;
		if(!generations.isEmpty())
		{
			SortedSet<Integer> keys = new TreeSet  <Integer>(generations.get(0).keySet());
			int max = keys.last();

			for(int j =1;j<generations.size();j++)
			{
				for(Integer score : generations.get(j).keySet())
				{
					if(max < score)
					{
						max = score;
						indiceGenerations = j;
					}
				}

			}
		}
		SortedSet<Integer> keys = new TreeSet  <Integer>(generations.get(indiceGenerations).keySet());


		System.out.println("MEILLEURE POPS FINALE : "+generations.get(indiceGenerations).get(keys.last()).score());
		return generations.get(indiceGenerations).get(keys.last());

	}
	private Server RecupServeurById(DataStruct data,int id)
	{
		for (int k = 0; k < data.getListServer().size(); k++) 
		{
			if(data.getListServer().get(k).getId() == id)
			{
				return data.getListServer().get(k);

			}
		}
		return null;
	}

	private void popServer(DataStruct data, Server serv)
	{

		for(int k = 0;k<serv.getNbEmpacement() -1;k++)
		{
			data.getListFreeSlots().get(serv.getRangee()).set(serv.getRangee()+k, true);
		}

		serv.setSlot(-1);
		serv.setRangee(-1);
		serv.setPool(-1);

	}


	private DataStruct RandomiseAffect(DataStruct data)
	{
		try
		{
			DataStruct datatmp = (DataStruct) data.clone();


			Collections.shuffle(datatmp.getListServer());
			int pool = 0;
			for(int i = 0; i<datatmp.getListServer().size();i++){

				for(int j = 0; j<datatmp.getListServer().get(i).getListSlotsPossible().size();j++){
					Boolean placeOccupe = false;
					int rangee = datatmp.getListServer().get(i).getListSlotsPossible().get(j).getRangee();
					int slot = datatmp.getListServer().get(i).getListSlotsPossible().get(j).getSlot();


					for(int k = 0; k<datatmp.getListServer().get(i).getNbEmpacement();k++){
						if(!datatmp.getListFreeSlots().get(rangee).get(slot+k)){
							placeOccupe = true;
						}
					}

					if(!placeOccupe){
						for(int k = 0; k<datatmp.getListServer().get(i).getNbEmpacement();k++){
							datatmp.getListFreeSlots().get(rangee).set(slot+k, false);
						}
						datatmp.getListServer().get(i).setRangee(rangee);
						datatmp.getListServer().get(i).setSlot(slot);
						datatmp.getListServer().get(i).setPool(pool);
						pool = (pool + 1)%datatmp.getNbPool();
						break;
					}
				}
			}
			return datatmp;
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public ArrayList<Integer> getScorePopulations() {
		return scorePopulations;
	}

	public void setScorePopulations(ArrayList<Integer> scorePopulations) {
		this.scorePopulations = scorePopulations;
	}

}
