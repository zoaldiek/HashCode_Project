package tools;

import java.util.Comparator;

import principale.Server;

public class TriRatio  implements Comparator<Server>{

	@Override
	public int compare(Server arg0, Server arg1) {
		return new Double(arg0.getRatio()).compareTo(arg1.getRatio());
	}

}