package tools;

import java.util.Comparator;


public class TriPool implements Comparator<Couple<Integer,Integer>>{

	@Override
	public int compare(Couple<Integer,Integer> arg0, Couple<Integer,Integer> arg1) {
		return new Integer(arg0.getRight()).compareTo(arg1.getRight());
	}

}
