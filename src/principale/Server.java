package principale;

import java.util.ArrayList;

public class Server implements Cloneable{
	private static int refId = 0;
	private int id;

	private int rangee;
	private int slot;
	private int pool;

	private int nbEmpacement;
	private int capacite;
	private double ratio;

	private ArrayList<Slot> listSlotsPossible; 

	public Server(int nbEmplacement, int capacite){
		this.id = refId;
		refId++;

		this.nbEmpacement = nbEmplacement;
		this.capacite = capacite;
		this.setRatio(this.capacite / this.nbEmpacement);

		this.rangee = -1;
		this.slot = -1;
		this.pool = -1;

		this.setListSlotsPossible(new ArrayList<>());
	}

	public int getNbEmpacement() {
		return nbEmpacement;
	}

	public int getCapacite() {
		return capacite;
	}

	public int getRangee() {
		return rangee;
	}

	public void setRangee(int rangee) {
		this.rangee = rangee;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}

	public ArrayList<Slot> getListSlotsPossible() {
		return listSlotsPossible;
	}

	public void setListSlotsPossible(ArrayList<Slot> listSlotsPossible) {
		this.listSlotsPossible = listSlotsPossible;
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	public void updateListSlotsPossible(ArrayList<Integer> listslotsoccupés, int rangee, int slot){
		//parcours de toute les rangées et toute les slots
		for(int i = 0; i<rangee; i++){
			for(int j = 0; j<slot-this.nbEmpacement+1; j++){
				boolean emplacementOK = true;
				for(int e = 0; e<this.nbEmpacement; e++){

					for(int k = 0; k<listslotsoccupés.size(); k=k+2){
						if((listslotsoccupés.get(k) == i) && (j+e == listslotsoccupés.get(k+1))){
							emplacementOK = false;
							break;
						}
					}
				}
				if(emplacementOK){
					listSlotsPossible.add(new Slot(i, j));
				}
			}

		}
	}

	@Override	
	//    public Object clone() throws CloneNotSupportedException {   
	//    	Server copie = (Server) super.clone();
	//    	
	//    	copie.listSlotsPossible = new ArrayList<>();
	//    	for(int i = 0; i < listSlotsPossible.size(); i++){
	//    		copie.listSlotsPossible.add(this.getListSlotsPossible().get(i));
	//    	}
	//    	return copie;
	//    }
	public Server clone() throws CloneNotSupportedException {   
		Server copie = new Server(this.nbEmpacement, this.capacite);
		copie.setId(id);
		copie.setListSlotsPossible(listSlotsPossible);
		copie.setPool(pool);
		copie.setRangee(rangee);
		copie.setRatio(this.ratio);
		copie.setSlot(slot);

		return copie;
	}

	public String toString(){
		String res = "Serveur "+this.id+": nbEmplacement = "+this.nbEmpacement+" || capacité = "+this.capacite+" || rangee = "+this.getRangee()+" || slot = " +this.slot+" || pool = "+this.pool+"\n";
		res += "Nombre de Slots possibles: "+listSlotsPossible.size();
		return res;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

}
