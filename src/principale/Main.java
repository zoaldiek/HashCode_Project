package principale;

import algorithmes.Glouton;
import algorithmes.MetaHeuris;
import algorithmes.PLNE;


public class Main {
	//A modifier pour 2 arguments en entré 
	private static int NbArguments = 2;
	public static String[] OPTION = {"-gb","-gp","-hb","-hp","-mb","-mp","-p"};


	public static void main(String[] args) throws CloneNotSupportedException {
		//Verification que le programme prend deux arguments sinon il renvoi un message d'erreur. 
		if(args.length != NbArguments){
			if(args.length<NbArguments-1){
				System.err.println("Pas assez d'argument: il requère 1 ou 2 arguments");
				return;
			}
			if(args.length>NbArguments){
				System.err.println("Trop d'argument: il requère 1 ou 2 arguments, vous en avez mis "+ args.length+".");
				return;
			}
		}

		//Verification que le programme prend les bons paramètres d'entrés
		boolean verif = false;
		for(int i=0;i<OPTION.length;i++){
			if(OPTION[i].equals(args[0])){
				verif = true;
			}
		}
		if(!verif){
			System.err.println("Argument 1 non valide veuillez vérifier le man page.");
			return;
		}

		DataStruct data;
		long t= System.currentTimeMillis();
		//Affichage de la data structure
		if(args.length == 2){
			data = ReadWriteView.readFile("ressources/dc.in", Float.parseFloat(args[1]));
			if(data == null){
				System.err.println("La valeur entrée doit être entre 0 et 100 (non inclu)");
			}
		}
		else{
			data = ReadWriteView.readFile("ressources/dc.in");
		}


		//Sélection de l'algorithme
		switch(args[0]){
		case "-gb":
			data = Glouton.Basic(data);
			break;
		case "-gp":
			data = Glouton.Perso(data);
			break;
		case "-mb":
			data = MetaHeuris.Basic(data);
			break;
		case "-mp":
			data = MetaHeuris.Perso(data);
			break;
		case "-p":
			data = PLNE.plne(data);
			break;
		case "-hb":
			data = PLNE.plne_relaxe(data);
			break;
		}
		System.out.println("temps de traitement en ms : "+Double.toString(System.currentTimeMillis()-t));
		
		ReadWriteView.writeFile(data);		

		for(int i = 0;i<data.getListServer().size();i++){
			System.out.println(data.getListServer().get(i));
		}
		int copa = 0;
		for(int i = 0; i<data.getListServer().size();i++){
			copa+= data.getListServer().get(i).getCapacite();
		}
		System.out.println("Capacité totale: "+copa);
		System.out.println();
		System.out.println("Le score est de: "+data.score());
		//ReadWriteView.viewFile(data);


	}
}