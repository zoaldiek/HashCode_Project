package principale;

import java.io.*;
import java.util.ArrayList;
import org.jdesktop.swingx.JXTable;
import javax.swing.*;
import java.awt.*;

public final class ReadWriteView {

	//Permet de lire le fichier sous forme demandé dans l'énoncé
	public static DataStruct readFile(String fileName){
		ArrayList<String> chaine = new ArrayList<>();

		try{
			InputStream ips = new FileInputStream(fileName);
			InputStreamReader iprs = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(iprs);

			String ligne;
			while((ligne=br.readLine())!=null){
				chaine.add(ligne);
			}
			br.close();
		}
		catch(Exception e){
			System.out.println("Erreur");
			System.err.println(e.getMessage());
		}

		int rangee;
		int slots;
		int nbIndispo;
		int nbPools;
		int nbServeurs;

		String[] temp = chaine.get(0).split(" ");
		rangee = Integer.parseInt(temp[0]);
		slots = Integer.parseInt(temp[1]);
		nbIndispo =Integer.parseInt(temp[2]);
		nbPools = Integer.parseInt(temp[3]);
		nbServeurs = Integer.parseInt(temp[4]);		

		ArrayList<Integer> listslotsoccupés = new ArrayList<>();
		ArrayList<Server> listServer = new ArrayList<>();

		for(int i = 1;i<=nbIndispo;i++){
			temp = chaine.get(i).split(" ");
			for(int j=0; j<temp.length;j++){
				listslotsoccupés.add(Integer.parseInt(temp[j]));
			}
		}

		for(int i = nbIndispo+1 ;i<= nbIndispo+nbServeurs;i++){
			temp = chaine.get(i).split(" ");
			for(int j=0; j<temp.length;j=j+2){
				listServer.add(new Server(Integer.parseInt(temp[j]),Integer.parseInt(temp[j+1])));
			}
		}

		
		return new DataStruct(rangee, slots, nbPools, listslotsoccupés, listServer);
	}

	//permet de lire un fichier en considérant un pourcentage p de l'instance
	public static DataStruct readFile(String fileName,float p){
		ArrayList<String> chaine = new ArrayList<>();

		if( p < 0 | p >= 100 )
		{
			System.err.println("Pourcentage non valide");
			return null;
		}

		try{
			InputStream ips = new FileInputStream(fileName);
			InputStreamReader iprs = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(iprs);

			String ligne;
			while((ligne=br.readLine())!=null){
				chaine.add(ligne);
			}
			br.close();
		}
		catch(Exception e){
			System.out.println("Erreur");
			System.err.println(e.getMessage());
		}

		int rangee;
		int slots;
		int nbIndispo;
		int nbPools;
		int nbServeurs;


		String[] temp = chaine.get(0).split(" ");
		//on change la valeurs des rangées et des pools ainsi que du nombre de serveur
		rangee = Integer.parseInt(temp[0]);
		rangee *= p/100;
		nbPools = Integer.parseInt(temp[3]);
		nbPools*= p/100;
		nbServeurs = Integer.parseInt(temp[4]);		
		nbServeurs*= p/100;
		//la valeur des slots ne change pas
		slots = Integer.parseInt(temp[1]);

		//on garde cette valeur comme borne supérieure
		nbIndispo =Integer.parseInt(temp[2]);




		ArrayList<Integer> listslotsoccupés = new ArrayList<>();
		ArrayList<Server> listServer = new ArrayList<>();



		for(int i = 1;i<=nbIndispo;i++){
			temp = chaine.get(i).split(" ");
			int tmpRangee = Integer.parseInt(temp[0]);
			int tmpSlot = Integer.parseInt(temp[1]);

			//si le numéro de la rangée n'est pas dans celles aceeptables on l'ignore
			if(!(tmpRangee >= rangee))
			{	
				listslotsoccupés.add(tmpRangee);
				listslotsoccupés.add(tmpSlot);

			}

		}
		//on reprend la lecture apres les maxIndispo indice 
		for(int i = nbIndispo+1 ;i<= nbIndispo+nbServeurs;i++){
			temp = chaine.get(i).split(" ");

			listServer.add(new Server(Integer.parseInt(temp[0]),Integer.parseInt(temp[1])));


		}

		

		return new DataStruct(rangee, slots, nbPools, listslotsoccupés, listServer);
	}


	//Permet d'ecrire dans un fichier la structure de retour
	public static void writeFile(DataStruct data){
		DataOutputStream dos;
		try{
			dos = new DataOutputStream(
					new BufferedOutputStream(
							new FileOutputStream(
									new File("ressources/res.txt"))));


			for(int i = 0; i<data.getListServer().size(); i++){
				if(data.getListServer().get(i).getRangee() != -1){
					dos.writeBytes(data.getListServer().get(i).getRangee()+" "+data.getListServer().get(i).getSlot()+" "+data.getListServer().get(i).getPool()+" ");
				}
				else{
					dos.writeBytes("x");
				}
				if(i!=data.getListServer().size()-1){
					dos.writeBytes("\n");
				}

			}
			dos.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}

		//Affichage dans un fichier
		try{
			dos = new DataOutputStream(
					new BufferedOutputStream(
							new FileOutputStream(
									new File("ressources/affichage.txt"))));

			for(int r = 0; r<data.getRangee(); r++){
				for(int s = 0; s<data.getSlots(); s++){
					boolean aucunElem = true;
					for(int i = 0; i<data.getListOcupySlots().size(); i=i+2){
						if(data.getListOcupySlots().get(i) ==r && data.getListOcupySlots().get(i+1)==s){
							aucunElem = false;
							dos.writeBytes("X ");
						}
					}
					for(int i = 0; i<data.getListServer().size(); i++){
						if(data.getListServer().get(i).getRangee() == r && data.getListServer().get(i).getSlot() == s){
							for(int nb = 0; nb<data.getListServer().get(i).getNbEmpacement(); nb++){
								dos.writeBytes(data.getListServer().get(i).getId()+"["+data.getListServer().get(i).getPool()+"]" + " ");
								s++;
							}
							aucunElem =false;
							s--;
						}
					}
					if(aucunElem){
						dos.writeBytes("- ");
					}

				}
				dos.writeBytes("\n");

			}
			dos.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/*2 onglets d'affichage -> un pour afficher les parametres d'entré -> Pour afficher la composition finale */
	public static void viewFile(DataStruct data){
		JFrame f = new JFrame("HashCode");
		f.setSize(1024, 760);

		JPanel pannel = new JPanel();
		JTabbedPane onglets = new JTabbedPane(SwingConstants.TOP);

		JPanel onglet1 = new JPanel();
		JLabel titreOnglet1 = new JLabel("Données d'entrée");
		onglet1.add(titreOnglet1);
		JTextArea texte = new JTextArea();
		texte.setBackground(Color.WHITE);
		texte.setText("Nombre de rangée : "+ data.getRangee()+"\nNombre de slot : "+ data.getSlots()+"\nNombre de pool : "+ data.getNbPool() + "\n"+"Le score est de : "+data.score());

		onglet1.add(texte);

		JPanel onglet2 = new JPanel();

		Object[][] donnees = new Object[data.getRangee()][data.getSlots()];
		String[] entetes = new String[data.getSlots()];

		for(int i = 0; i<data.getRangee(); i++){
			for(int j = 0; j<data.getSlots(); j++){
				donnees[i][j] = "-";
			}
		}


		for(int r = 0; r<data.getSlots(); r++){
			entetes[r] = Integer.toString(r);
		}

		for(int s = 0; s<data.getListOcupySlots().size(); s=s+2){
			donnees[data.getListOcupySlots().get(s)][data.getListOcupySlots().get(s+1)] = "X";
		}

		for(int i = 0; i<data.getListServer().size(); i++){
			if(data.getListServer().get(i).getRangee()!=-1){
				for(int j = data.getListServer().get(i).getSlot(); j< data.getListServer().get(i).getSlot()+data.getListServer().get(i).getNbEmpacement();j++){
					donnees[data.getListServer().get(i).getRangee()][j] = Integer.toString(data.getListServer().get(i).getId());
				}
			}
		}

		JXTable tableau = new JXTable(donnees, entetes);
		tableau.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
		tableau.packAll();
		tableau.setAutoscrolls(true);
		onglet2.add(tableau.getTableHeader(), BorderLayout.NORTH);
		onglet2.add(tableau, BorderLayout.CENTER);

		onglets.addTab("entrée", onglet1);
		onglets.addTab("sortie", onglet2);
		onglets.setOpaque(true);
		pannel.add(onglets);
		f.getContentPane().add(pannel);

		f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		/*Centré la fenêtre*/
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation(dim.width/2 - f.getWidth()/2, dim.height/2 - f.getHeight()/2);
		f.setVisible(true);
	}
}
