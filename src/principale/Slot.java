package principale;

public class Slot{
	private int rangee;
	private int slot;
	
	public Slot(int rangee, int slot){
		this.rangee = rangee;
		this.slot = slot;
	}

	public int getRangee() {
		return rangee;
	}

	public void setRangee(int rangee) {
		this.rangee = rangee;
	}

	public int getSlot() { 
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}
	
}
