package principale;

import java.util.ArrayList;
import java.util.Collections;

public class DataStruct implements Cloneable{
	
	// Nombre de rangées 
	private Integer rangee; 
	
	// Nombre de slots par rangée 
	private Integer slots;
	
	//Capacité total de tous les serveurs
	private int capaciteTotal;
	
	// Slots indisponibles (r1, s1, r2, s2,  .... , ri, si)
	private ArrayList<Integer> listOcupySlots = new ArrayList<>();
	
	// Nombre de Pool à créer
	private Integer nbPool;
	
	// Ensemble des serveurs (classe Server)
	private ArrayList<Server> listServer = new ArrayList<>();
	
	/////////// Initialisé dans le constructeur /////////////
	
	// true si disponible , false si indisponible
	private ArrayList<ArrayList<Boolean>> listFreeSlots = new ArrayList<>();
	
	private ArrayList<ArrayList<ArrayList<Integer>>> listServerParSlots = new ArrayList<>();
	
	
	public DataStruct(int rangee, int slots, int nombrePool, ArrayList<Integer> listslotsoccupés, ArrayList<Server> listServer)
	{
		this.rangee = rangee;
		this.slots = slots;
		nbPool = nombrePool;
		listOcupySlots = listslotsoccupés;
		this.listServer = listServer;
		
		//initialise les slots possibles pour tous les serveurs
		for(int i = 0;i<listServer.size();i++){
			listServer.get(i).updateListSlotsPossible(listslotsoccupés, rangee, slots);
			this.setCapaciteTotal(this.getCapaciteTotal() + listServer.get(i).getCapacite());
		}
		this.initProject();
		
	}
	
	private void initProject()
	{
		//initialisation a True de listFreeSlot
		for(int r = 0; r < this.rangee;r++)
		{
			ArrayList<Boolean> listFreeSlot = new ArrayList<>();
			for (int s = 0; s < this.slots; s++) 
			{
				listFreeSlot.add(true);
			}
			this.listFreeSlots.add(listFreeSlot);
		}
		
		//update de listFreeSlot
		for(int i=0; i < listOcupySlots.size(); i = i+2){
			this.listFreeSlots.get(listOcupySlots.get(i)).set(listOcupySlots.get(i+1), false);
		}
		
		//initialisation de listServerParSlots
		ArrayList<ArrayList<Integer>> slots = null;
		ArrayList<Integer> server = null;
		for(int r = 0;r<this.rangee; r++){
			slots = new ArrayList<>();
			for(int s = 0;s<this.slots; s++){
				 server = new ArrayList<>();
				 slots.add(server);
			}
			listServerParSlots.add(slots);
		}
		
		//Update de listServerParSlots
		for(int i = 0; i<listServer.size();i++ ){
			for(int j = 0; j<listServer.get(i).getListSlotsPossible().size();j++){
				ArrayList<Slot> tempListSlot = listServer.get(i).getListSlotsPossible();
				listServerParSlots.get(tempListSlot.get(j).getRangee()).get(tempListSlot.get(j).getSlot()).add(listServer.get(i).getId());
			}
		}
	}
	
	public Integer score(){
		ArrayList<Integer> listGCi = new ArrayList<>();
		for(int i = 0; i<this.getNbPool();i++){
			listGCi.add(this.gci(i));
		}
		return Collections.min(listGCi);
	}
	
	public Integer gci(int pool){
		ArrayList<Integer> listGCi = new ArrayList<>();
		for(int r = 0; r<this.getRangee();r++){
			int gci = 0;
			for(int ra = 0; ra<this.getRangee();ra++){
				if(r != ra){
					 for(int serv = 0; serv<this.getListServer().size();serv++){
						 if(this.getListServer().get(serv).getRangee() == ra){
							 if(this.getListServer().get(serv).getPool() == pool){
								 gci += this.getListServer().get(serv).getCapacite();
							 }
						 }
					 }
				}
			}
			listGCi.add(gci);
		}
		return Collections.min(listGCi);
	}
	
	
	@Override	
    public Object clone() throws CloneNotSupportedException {   
    	DataStruct copie = (DataStruct)super.clone();
 
    	copie.listServer = new ArrayList<>();
    	for(int i = 0; i<listServer.size(); i++){
    		copie.listServer.add((Server) listServer.get(i).clone());
    		copie.listServer.get(i).setId(listServer.get(i).getId());
    	}
    	
    	copie.listServerParSlots = new ArrayList<>();
    	for(int i = 0; i<listServerParSlots.size(); i++){
    		copie.listServerParSlots.add(new ArrayList<>());
    		for(int j = 0; j< listServerParSlots.get(i).size();j++){
    			copie.listServerParSlots.get(i).add(new ArrayList<>());
    			for(int k = 0; k<listServerParSlots.get(i).get(j).size();k++ ){
    				copie.listServerParSlots.get(i).get(j).add(listServerParSlots.get(i).get(j).get(k));
    			}
    		}
    	}
    	
    	copie.listFreeSlots = new ArrayList<>();
    	for(int i = 0;i<listFreeSlots.size(); i++){
    		copie.listFreeSlots.add(new ArrayList<>());
    		for(int j = 0;j<listFreeSlots.get(i).size(); j++){
    			copie.listFreeSlots.get(i).add(listFreeSlots.get(i).get(j));
    		}
    	}

    	return copie;
    }

	public Integer getRangee() {
		return rangee;
	}


	public void setRangee(Integer rangee) {
		this.rangee = rangee;
	}


	public Integer getSlots() {
		return slots;
	}


	public void setSlots(Integer slots) {
		this.slots = slots;
	}


	public ArrayList<Integer> getListOcupySlots() {
		return listOcupySlots;
	}


	public void setListOcupySlots(ArrayList<Integer> listOcupySlots) {
		this.listOcupySlots = listOcupySlots;
	}


	public Integer getNbPool() {
		return nbPool;
	}


	public void setNbPool(Integer nbPool) {
		this.nbPool = nbPool;
	}


	public ArrayList<Server> getListServer() {
		return listServer;
	}


	public void setListServer(ArrayList<Server> listServer) {
		this.listServer = listServer;
	}

	public ArrayList<ArrayList<Boolean>> getListFreeSlots() {
		return listFreeSlots;
	}

	public void setListFreeSlots(ArrayList<ArrayList<Boolean>> listFreeSlots) {
		this.listFreeSlots = listFreeSlots;
	}

	public ArrayList<ArrayList<ArrayList<Integer>>> getListServerParSlots() {
		return listServerParSlots;
	}

	public void setListServerParSlots(ArrayList<ArrayList<ArrayList<Integer>>> listServerParSlots) {
		this.listServerParSlots = listServerParSlots;
	}

	public int getCapaciteTotal() {
		return capaciteTotal;
	}

	public void setCapaciteTotal(int capaciteTotal) {
		this.capaciteTotal = capaciteTotal;
	}
				
}
