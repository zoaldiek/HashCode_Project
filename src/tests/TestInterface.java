package tests;

import java.awt.Color;
import ihm.WindowLancement;
import principale.ReadWriteView;

public class TestInterface {

	public static void main(String[] args) {
		WindowLancement charge = new WindowLancement("ressources/sorbonne.png", Color.WHITE);
		charge.setVisible(true);
		charge.run();
		int progression = 0;
		for(int i = 0; i<35; i++ ){
			charge.avancer(progression, "Chargement de la base de donnée");
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			progression++;
			if(progression==20){
				charge.changeImage("ressources/upmc.png");
			}
		}
		for(int i = 0; i<35; i++ ){
			charge.avancer(progression, "Chargement de l'algorithme");
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(progression==40){
				charge.changeImage("ressources/hashcode.jpg");
			}
			progression++;
		}
		for(int i=0;i<30;i++){
			charge.avancer(progression, "Traitement des données");
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			progression++;
		}
		charge.dispose();
		ReadWriteView.viewFile(null);
	}
}
