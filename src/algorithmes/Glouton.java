package algorithmes;

import java.util.ArrayList;
import java.util.Collections;
import principale.DataStruct;
import principale.Server;
import tools.Couple;
import tools.TriCompare;
import tools.TriId;
import tools.TriNbEmplacement;
import tools.TriRatio;
import tools.TriServeur;

public final class Glouton {

	public static DataStruct Basic(DataStruct data) {
		Glouton.triReverseCapacity(data);

		int pool = 0;
		// Parcours de tous les serveurs dans l'ordre décroissant (capacité)
		for (int i = 0; i < data.getListServer().size(); i++) {

			// Pour plus de robustesse, il faudrait les classer par ordres
			// croissants
			for (int j = 0; j < data.getListServer().get(i).getListSlotsPossible().size(); j++) {
				Boolean placeOccupe = false;
				int rangee = data.getListServer().get(i).getListSlotsPossible().get(j).getRangee();
				int slot = data.getListServer().get(i).getListSlotsPossible().get(j).getSlot();

				// Parcours des emplacements pour savoir s'ils sont libres
				for (int k = 0; k < data.getListServer().get(i).getNbEmpacement(); k++) {
					if (!data.getListFreeSlots().get(rangee).get(slot + k)) {
						placeOccupe = true;
					}
				}

				if (!placeOccupe) {
					for (int k = 0; k < data.getListServer().get(i).getNbEmpacement(); k++) {
						data.getListFreeSlots().get(rangee).set(slot + k, false);
					}
					data.getListServer().get(i).setRangee(rangee);
					data.getListServer().get(i).setSlot(slot);
					data.getListServer().get(i).setPool(pool);
					pool = (pool + 1) % data.getNbPool();
					break;
				}
			}
		}

		// Il est important d'avoir la liste des serveurs par ordre croissant
		// pour les prochains algorithme
		Glouton.triId(data);

		return data;
	}

	public static DataStruct Perso(DataStruct data) {
		// Calcul de la moyenne des capacites par serveur
		// Calcul de la moyenne du nombre emplacement par serveur
		int total = 0;
		int emplac = 0;
		for (int serv = 0; serv < data.getListServer().size(); serv++) {
			total += data.getListServer().get(serv).getCapacite();
			emplac += data.getListServer().get(serv).getNbEmpacement();
		}

		int capParServeur = total / data.getListServer().size();
		int nbEmplParServeur = emplac / data.getListServer().size();
		System.out.println(nbEmplParServeur);

		int nbEmplDispo = data.getRangee() * data.getSlots();

		int nbEmplParPool = nbEmplDispo / data.getNbPool();

		int nbMoyEmplParPool = nbEmplParPool / nbEmplParServeur;

		// Tri des serveurs possédant des capacités superieurs à la moyenne
		ArrayList<Server> serverOP = new ArrayList<>();
		ArrayList<Server> serverNULL = new ArrayList<>();
		for (int serv = 0; serv < data.getListServer().size(); serv++) {
			if ((data.getListServer().get(serv).getCapacite() >= capParServeur - 2)
					&& (data.getListServer().get(serv).getNbEmpacement() <= nbEmplParServeur +2)) {
				serverOP.add(data.getListServer().get(serv));
			} else {
				serverNULL.add(data.getListServer().get(serv));
			}
		}

		// Placement des serveurs OP
		Collections.sort(serverOP, new  TriServeur().reversed());

		int pool = 0;
		ArrayList<Couple<Integer, Integer>> poolCapacity = new ArrayList<>();
		for (int i = 0; i < data.getNbPool(); i++) {
			poolCapacity.add(new Couple<Integer, Integer>(i, 0));
		}
		int rangee = 0;
		
		for (int i = 0; i < serverOP.size(); i++) {
			
			// Pour plus de robustesse, il faudrait les classer par ordres
			// croissants
			for (int j = 0; j < serverOP.get(i).getListSlotsPossible().size(); j++) {
				Boolean placeOccupe = false;
				int slot = serverOP.get(i).getListSlotsPossible().get(j).getSlot();

				// Parcours des emplacements pour savoir s'ils sont libres
				for (int k = 0; k < serverOP.get(i).getNbEmpacement(); k++) {
					if (!data.getListFreeSlots().get(rangee).get(slot + k)) {
						placeOccupe = true;
					}
				}

				for (int r = 0; r < data.getRangee(); r++) {

				}

				if (!placeOccupe) {
					for (int k = 0; k < serverOP.get(i).getNbEmpacement(); k++) {
						data.getListFreeSlots().get(rangee).set(slot + k, false);
					}
					if (poolCapacity.get(pool).getRight() > capParServeur * (nbMoyEmplParPool - 1)){
						pool = (pool + 1) % data.getNbPool();
					}
					serverOP.get(i).setRangee(rangee);
					serverOP.get(i).setSlot(slot);
					serverOP.get(i).setPool(pool);
					poolCapacity.get(pool).setRight(poolCapacity.get(pool).getRight() + serverOP.get(i).getCapacite());
					rangee = (rangee + 1) % data.getRangee();
					break;
				}

			}

		}
		Collections.sort(serverNULL, new  TriServeur().reversed());
		for (int i = 0; i < serverNULL.size(); i++) {

			// Pour plus de robustesse, il faudrait les classer par ordres
			// croissants
			for (int j = 0; j < serverNULL.get(i).getListSlotsPossible().size(); j++) {
				Boolean placeOccupe = false;
				int slot = serverNULL.get(i).getListSlotsPossible().get(j).getSlot();

				// Parcours des emplacements pour savoir s'ils sont libres
				for (int k = 0; k < serverNULL.get(i).getNbEmpacement(); k++) {
					if (!data.getListFreeSlots().get(rangee).get(slot + k)) {
						placeOccupe = true;
					}
				}

				if (!placeOccupe) {
					for (int k = 0; k < serverNULL.get(i).getNbEmpacement(); k++) {
						data.getListFreeSlots().get(rangee).set(slot + k, false);
					}
					if (poolCapacity.get(pool).getRight() > capParServeur * (nbMoyEmplParPool - 1)) {
						pool = (pool + 1) % data.getNbPool();
					}
					serverNULL.get(i).setRangee(rangee);
					serverNULL.get(i).setSlot(slot);
					serverNULL.get(i).setPool(pool);
					poolCapacity.get(pool).setRight(poolCapacity.get(pool).getRight() + serverNULL.get(i).getCapacite());
					rangee = (rangee + 1) % data.getRangee();
					break;
				}

			}

		}

		return data;
	}

	public static DataStruct triReverseCapacity(DataStruct data) {
		Collections.sort(data.getListServer(), new TriCompare().reversed());
		return data;
	}

	public static DataStruct triId(DataStruct data) {
		Collections.sort(data.getListServer(), new TriId());
		return data;
	}

	public static DataStruct triNbEmplacement(DataStruct data) {
		Collections.sort(data.getListServer(), new TriNbEmplacement().reversed());
		return data;
	}

	public static DataStruct triReverseRatio(DataStruct data) {
		Collections.sort(data.getListServer(), new TriRatio().reversed());
		return data;
	}

}
