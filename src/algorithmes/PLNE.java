package algorithmes;

import gurobi.*;
import principale.DataStruct;

public final class PLNE {

	public static DataStruct plne(DataStruct data){
		try{
			GRBEnv env = new GRBEnv("hashCode.log");
			GRBModel model = new GRBModel(env);
			model.set(GRB.DoubleParam.TimeLimit, 60*3);
			
			// Create variables
			GRBVar score = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "score");
			
			GRBVar [][][][] z = new GRBVar[data.getListServer().size()][data.getRangee()][data.getSlots()][data.getNbPool()];
			for(int m = 0; m<z.length; m++){
				for(int r = 0; r<z[m].length; r++){
					for(int s = 0; s<z[m][r].length; s++){
						for(int i = 0; i<z[m][r][s].length; i++){							
							for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
								if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r && data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
									z[m][r][s][i] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "Z"+m+"_"+r+"_"+s+"_"+i);
								}
							}
							
						}
					}
				}
			}

			// Set objective
			GRBLinExpr obj = new GRBLinExpr();
		    obj.addTerm(1.0, score);
		    model.setObjective(obj, GRB.MAXIMIZE);
							
			// Add constraint 1
		    for(int r1 = 0; r1<data.getRangee(); r1++){
				for(int i = 0; i < data.getNbPool(); i++){
					GRBLinExpr expr = new GRBLinExpr();
					for(int r = 0;r<data.getRangee(); r++){
						if(r != r1){
							for(int s = 0; s<data.getSlots();s++){
								for(int m = 0; m< data.getListServer().size(); m++){
									for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
										if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
											expr.addTerm(data.getListServer().get(m).getCapacite(), z[m][r][s][i]);
										}
									}
								}
							}
						}
					}
					expr.addTerm(-1, score);
					model.addConstr(expr, GRB.GREATER_EQUAL, 0, "c1_"+r1);
				}
		    }
		    
		    //Add contrainst 2
		    for(int m = 0; m<data.getListServer().size(); m++){
		    	GRBLinExpr expr = new GRBLinExpr();
		    	for(int r =0; r<data.getRangee();r++){
		    		for(int s = 0; s<data.getSlots();s++){
		    			for(int i = 0; i<data.getNbPool();i++){	
		    				for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
								if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
									expr.addTerm(1, z[m][r][s][i]);
								}
		    				}
		    			}
		    		}
		    	}
		    	model.addConstr(expr, GRB.LESS_EQUAL, 1, "c2_"+m);
		    }
		    
		    //Add constrainst 3
		    for(int r = 0; r<data.getRangee(); r++){
		    	for(int s = 0; s<data.getSlots(); s++){
		    		GRBLinExpr expr = new GRBLinExpr();
		    		for(int m = 0; m<data.getListServer().size();m++){
		    			for(int i = 0; i<data.getNbPool(); i++){
		    				for(int l = 0; l<data.getListServer().get(m).getNbEmpacement();l++){
			    				for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
									if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s+l){ 
										expr.addTerm(1, z[m][r][s+l][i]);
									}
			 					}
		    				}
		    			}
		    		}
		    		model.addConstr(expr, GRB.LESS_EQUAL, 1, "c3_"+r+"_"+s);
		    	}
		    }
			
			// Optimize model
			model.optimize();
			
			for(int m = 0; m<data.getListServer().size(); m++){
				for(int r = 0; r<data.getRangee(); r++){
					for(int s = 0; s<data.getSlots(); s++){
						for(int i = 0; i<data.getNbPool(); i++){
							for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
								if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
									if(z[m][r][s][i].get(GRB.DoubleAttr.X) == 1){
										data.getListServer().get(m).setRangee(r);
										data.getListServer().get(m).setSlot(s);
										data.getListServer().get(m).setPool(i);
									}
								}
		    				}
							
						}
					}
				}
			}
			
			// Dispose of model and environment
			model.dispose();
			env.dispose();
		}
		catch(GRBException e){
			System.out.println("Error code: "+ e.getErrorCode() +". "+e.getMessage());
		}
		
		return data;
	}
	
	
	
	public static DataStruct plne_relaxe(DataStruct data){
		try{
			GRBEnv env = new GRBEnv("hashCode.log");
			GRBModel model = new GRBModel(env);
			model.set(GRB.DoubleParam.TimeLimit, 60*3);
			
			// Create variables
			GRBVar score = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "score");
			
			GRBVar [][][][] z = new GRBVar[data.getListServer().size()][data.getRangee()][data.getSlots()][data.getNbPool()];
			for(int m = 0; m<z.length; m++){
				for(int r = 0; r<z[m].length; r++){
					for(int s = 0; s<z[m][r].length; s++){
						for(int i = 0; i<z[m][r][s].length; i++){							
							for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
								if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r && data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
									z[m][r][s][i] = model.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "Z"+m+"_"+r+"_"+s+"_"+i);
								}
							}
							
						}
					}
				}
			}

			// Set objective
			GRBLinExpr obj = new GRBLinExpr();
		    obj.addTerm(1.0, score);
		    model.setObjective(obj, GRB.MAXIMIZE);
							
			// Add constraint 1
		    for(int r1 = 0; r1<data.getRangee(); r1++){
				for(int i = 0; i < data.getNbPool(); i++){
					GRBLinExpr expr = new GRBLinExpr();
					for(int r = 0;r<data.getRangee(); r++){
						if(r != r1){
							for(int s = 0; s<data.getSlots();s++){
								for(int m = 0; m< data.getListServer().size(); m++){
									for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
										if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
											expr.addTerm(data.getListServer().get(m).getCapacite(), z[m][r][s][i]);
										}
									}
								}
							}
						}
					}
					expr.addTerm(-1, score);
					model.addConstr(expr, GRB.GREATER_EQUAL, 0, "c1_"+r1);
				}
		    }
		    
		    //Add contrainst 2
		    for(int m = 0; m<data.getListServer().size(); m++){
		    	GRBLinExpr expr = new GRBLinExpr();
		    	for(int r =0; r<data.getRangee();r++){
		    		for(int s = 0; s<data.getSlots();s++){
		    			for(int i = 0; i<data.getNbPool();i++){	
		    				for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
								if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
									expr.addTerm(1, z[m][r][s][i]);
								}
		    				}
		    			}
		    		}
		    	}
		    	model.addConstr(expr, GRB.LESS_EQUAL, 1, "c2_"+m);
		    }
		    
		    //Add constrainst 3
		    for(int r = 0; r<data.getRangee(); r++){
		    	for(int s = 0; s<data.getSlots(); s++){
		    		GRBLinExpr expr = new GRBLinExpr();
		    		for(int m = 0; m<data.getListServer().size();m++){
		    			for(int i = 0; i<data.getNbPool(); i++){
		    				for(int l = 0; l<data.getListServer().get(m).getNbEmpacement();l++){
			    				for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
									if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s+l){ 
										expr.addTerm(1, z[m][r][s+l][i]);
									}
			 					}
		    				}
		    			}
		    		}
		    		model.addConstr(expr, GRB.LESS_EQUAL, 1, "c3_"+r+"_"+s);
		    	}
		    }
			
			// Optimize model
			model.optimize();
			
			for(int m = 0; m<data.getListServer().size(); m++){
				for(int r = 0; r<data.getRangee(); r++){
					for(int s = 0; s<data.getSlots(); s++){
						for(int i = 0; i<data.getNbPool(); i++){
							for(int slotLibre = 0; slotLibre<data.getListServer().get(m).getListSlotsPossible().size();slotLibre++){
								if(data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getRangee() == r &&data.getListServer().get(m).getListSlotsPossible().get(slotLibre).getSlot() == s){
									
								}
		    				}
							
						}
					}
				}
			}
			
			// Dispose of model and environment
			model.dispose();
			env.dispose();
		}
		catch(GRBException e){
			System.out.println("Error code: "+ e.getErrorCode() +". "+e.getMessage());
		}
		
		return data;
	}
	
}
