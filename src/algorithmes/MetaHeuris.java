package algorithmes;

import java.util.ArrayList;
import java.util.Random;

import principale.DataStruct;
import principale.Slot;
import tools.AlgoGenetique;

public final class MetaHeuris {
	
	//Structurer en 3 parties -> soit appel de fonction, soit directement ici!
	public static DataStruct Basic(DataStruct data) throws CloneNotSupportedException{
		double proba = 0;
		double probaA = 0.30;
		double probaB = 0.30;
		double probaC = 0.40;
		
		int tour = 0;
		int nbTours = 5000;
		Random rand = new Random();
		
		DataStruct newData = null;
		
		if(probaA + probaB + probaC != 1){
			System.err.println("La somme des probabilités n'est pas égale à 1 => Modifier les valeurs dans MetaHeuris.java");
			System.exit(1);
		}
		
		//Initialisation avec méthode glouton (A modifier quand Glouton.perso sera implémenté)
		data = Glouton.Perso(data);
		newData = (DataStruct) data.clone();
		
		//Itération de l'algorithme
		while(tour < nbTours){
			//Tirage aléatoire entre 0 et 1
			proba = rand.nextDouble();
			
			//ProbaA
			if(proba < probaA){
				ArrayList<Integer> listServAffect = new ArrayList<>();
				
				for(int serv = 0; serv<newData.getListServer().size(); serv++){
					if(newData.getListServer().get(serv).getRangee() != -1){
						listServAffect.add(newData.getListServer().get(serv).getId());
					}
				}
				
				int servSupp = listServAffect.get(rand.nextInt(listServAffect.size()));
				
				for(int s = 0; s<newData.getListServer().get(servSupp).getNbEmpacement(); s++){
					newData.getListFreeSlots().get(newData.getListServer().get(servSupp).getRangee()).set((newData.getListServer().get(servSupp).getSlot()+s), true);
				}
				
				newData.getListServer().get(servSupp).setPool(-1);
				newData.getListServer().get(servSupp).setRangee(-1);
				newData.getListServer().get(servSupp).setSlot(-1);
				
				
			}
			//ProbaB
			// probleme sur un tableau
			else if(proba  < probaA +probaB){
				boolean emplacementLibre = false;
				
				int servAjout = 0;
				
				ArrayList<Integer> listServNonAffect = new ArrayList<>();
				ArrayList<Slot> slotsPossible = new ArrayList<>();
				ArrayList<Slot> slotsDipo = new ArrayList<>();
				
				for(int serv = 0; serv<newData.getListServer().size(); serv++){
					if(newData.getListServer().get(serv).getRangee() == -1){
						listServNonAffect.add(newData.getListServer().get(serv).getId());
					}
				}
				
				while(slotsDipo.isEmpty() && !listServNonAffect.isEmpty()){
					int randomIndex = rand.nextInt(listServNonAffect.size());
					servAjout = listServNonAffect.get(randomIndex);
					
					slotsPossible = newData.getListServer().get(servAjout).getListSlotsPossible();
					for(int s = 0; s<slotsPossible.size(); s++){
						emplacementLibre = true;
						for(int i = 0; i<newData.getListServer().get(servAjout).getNbEmpacement(); i++){
							if(!newData.getListFreeSlots().get(slotsPossible.get(s).getRangee()).get(slotsPossible.get(s).getSlot()+i)){
								emplacementLibre = false;
							}
						}
						if(emplacementLibre){
							slotsDipo.add(slotsPossible.get(s));
						}
					}
					if(slotsDipo.isEmpty()){
						listServNonAffect.remove(randomIndex);
					}
				}
				
				if(!slotsDipo.isEmpty()){
					Slot slotSelect = slotsDipo.get(rand.nextInt(slotsDipo.size()));
					for(int s = 0; s<newData.getListServer().get(servAjout).getNbEmpacement(); s++){
						newData.getListFreeSlots().get(slotSelect.getRangee()).set((slotSelect.getSlot()+s), false);
					}
					newData.getListServer().get(servAjout).setPool(rand.nextInt(newData.getNbPool()));
					newData.getListServer().get(servAjout).setRangee(slotSelect.getRangee());
					newData.getListServer().get(servAjout).setSlot(slotSelect.getSlot());
				}
			}
			//ProbaC
			else{
				boolean emplacementLibre = false;
				
				int servAjout = 0;
				
				ArrayList<Integer> listServAffect = new ArrayList<>();
				ArrayList<Slot> slotsPossible = new ArrayList<>();
				ArrayList<Slot> slotsDipo = new ArrayList<>();
				
				for(int serv = 0; serv<newData.getListServer().size(); serv++){
					if(newData.getListServer().get(serv).getRangee() != -1){
						listServAffect.add(newData.getListServer().get(serv).getId());
					}
				}
				
				while(slotsDipo.isEmpty() && !listServAffect.isEmpty()){
					servAjout = listServAffect.get(rand.nextInt(listServAffect.size()));
					
					slotsPossible = newData.getListServer().get(servAjout).getListSlotsPossible();
					for(int s = 0; s<slotsPossible.size(); s++){
						emplacementLibre = true;
						for(int i = 0; i<newData.getListServer().get(servAjout).getNbEmpacement(); i++){
							if(!newData.getListFreeSlots().get(slotsPossible.get(s).getRangee()).get(slotsPossible.get(s).getSlot()+i)){
								emplacementLibre = false;
							}
						}
						if(emplacementLibre){
							slotsDipo.add(slotsPossible.get(s));
						}
					}
					if(slotsDipo.isEmpty()){
						listServAffect.remove(listServAffect.indexOf(servAjout));
					}
				}
				
				if(!slotsDipo.isEmpty()){
					Slot slotSelect = slotsDipo.get(rand.nextInt(slotsDipo.size()));
					for(int s = 0; s<newData.getListServer().get(servAjout).getNbEmpacement(); s++){
						newData.getListFreeSlots().get(newData.getListServer().get(servAjout).getRangee()).set(newData.getListServer().get(servAjout).getSlot(), true);						
					}
					
					for(int s = 0; s<newData.getListServer().get(servAjout).getNbEmpacement(); s++){
						newData.getListFreeSlots().get(slotSelect.getRangee()).set((slotSelect.getSlot()+s), false);						
					}

					newData.getListServer().get(servAjout).setPool(rand.nextInt(newData.getNbPool()));
					newData.getListServer().get(servAjout).setRangee(slotSelect.getRangee());
					newData.getListServer().get(servAjout).setSlot(slotSelect.getSlot());
				}
			}
					
			if(newData.score() < data.score()){
				newData = (DataStruct) data.clone();
			}
			else{
				data = (DataStruct) newData.clone();
			}
			tour++;
		}
		return data;
	}
	
	public static DataStruct Perso(DataStruct data){
		AlgoGenetique algo = new AlgoGenetique(data);
		try {
			return algo.run();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
}
